clear

isBash=$(echo $SHELL)
if [ $isBash != "/bin/bash" ];
then
  # Early exit if not bash shell !
  echo "This extensions were only tested on bash shell. Exiting." 1>&2
  # Unspecified error
  exit 1
fi

mkdir -p ~/my_scripts_bash
cp src/* ~/my_scripts_bash

isInFile=$(cat ~/.bashrc | grep -c "export PATH=my_scripts_bash:\${PATH}")
if [ $isInFile -eq 0 ];
then
  echo '# Include my_scripts_bash extensions in PATH' >> ~/.bashrc
  echo 'export PATH=my_scripts_bash:${PATH}' >> ~/.bashrc
else
  echo 'no need to modify .bashrc file. New files copied.'
fi
# alternative code
#path=~/bin            # path to be included
#bashrc=~/.bashrc      # bash file to be written and reloaded
# run the following code unmodified
#echo $PATH | grep -q "\(^\|:\)$path\(:\|/\{0,1\}$\)" || echo "PATH=\$PATH:$path" >> "$bashrc"; source "$bashrc"