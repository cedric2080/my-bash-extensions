url=$1

if [ -z "$2" ]
  then
    odir=~/Downloads
else
    odir=$2
fi

while [ 1 ]; do
    wget --retry-connrefused --waitretry=1 --read-timeout=20 --timeout=15 -t 0 --continue ${url} -P ${odir}
    if [ $? = 0 ]; then break; fi; # check return value, break if successful (0)
    sleep 1s;
done;
