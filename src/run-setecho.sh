# CRE
# Configuring a bit the echo system :))
echo_parameter=
echo_e=echo $echo_parameter
# printf "I ${RED}love${NC} Stack Overflow\n" # Now we know where I picked this color definition command :D
PUR='\033[0;35m'
BLU='\033[0;34m'
GRE='\033[0;32m'
RED='\033[0;31m'
REDINVBLINK='\033[31;5;7m'
REDINV='\033[31;7m'
GREINV='\033[32;7m'
NC='\033[0m' # No Color
$echo_e "${GRE}Echo system configured.${NC}"