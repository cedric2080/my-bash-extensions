#!/usr/bin/env bash
# force interactive for the source of bashrc to use parameters #!/bin/bash -i
# ref: https://askubuntu.com/questions/64387/cannot-successfully-source-bashrc-from-a-shell-script
clear

echo "OpenFOAM distribution, installation starting."

# configure main folder for OpenFOAM
ofoam_ver=10
helyxos_ver=2.4.0
distdir=${HOME}/OpenFOAM
. run-setecho.sh

# TODO check if distdir + ofoam_ver exists and provide the user a chance to exit

mkdir -p ${distdir}
cd ${distdir}
$echo_e "${GRE}OpenFOAM folder created.${NC}"

# get and move installers
$echo_e "${BLU}Downloading and extracting installers and source codes.${NC}"
wget https://github.com/ENGYS/HELYX-OS/releases/download/v${helyxos_ver}/HELYX-OS-${helyxos_ver}-linux-x86_64.bin
# TODO treat errors
chmod 777 HELYX-OS-${helyxos_ver}-linux-x86_64.bin
$echo_e "${GRE}HELYX-OS installer downloaded.${NC}"

wget http://dl.openfoam.org/source/${ofoam_ver}
# TODO treat errors
tar xvf ${ofoam_ver}
rm ${ofoam_ver}
mv OpenFOAM-${ofoam_ver}-version-${ofoam_ver} OpenFOAM-${ofoam_ver}

wget http://dl.openfoam.org/third-party/${ofoam_ver}
# TODO treat errors
tar xvf ${ofoam_ver}
rm ${ofoam_ver}
mv ThirdParty-${ofoam_ver}-version-${ofoam_ver} ThirdParty-${ofoam_ver}
$echo_e "${GRE}OpenFOAM version ${ofoam_ver} distribution downloaded.${NC}"

# install dependencies
$echo_e "${BLU}Installing System dependencies.${NC}"
sudo apt-get install build-essential cmake git ca-certificates
sudo apt-get install flex libfl-dev bison zlib1g-dev libboost-system-dev libboost-thread-dev libopenmpi-dev openmpi-bin gnuplot libreadline-dev libncurses-dev libxt-dev
sudo apt install curl libqt5x11extras5-dev libxt-dev qt5-quick-demos qt5ct qttools5-dev qtbase5-dev time
$echo_e "${GRE}System dependencies installed.${NC}"

# resolve varenvs
#source ${distdir}/OpenFOAM-${ofoam_ver}/etc/bashrc
# add this line to .bashrc and then source .bashrc
# include .bashrc if it exists
$echo_e "${BLU}Configuring VARENVS.${NC}"
isInFile=$(cat $HOME/.bashrc | grep -c "/OpenFOAM-${ofoam_ver}/etc/bashrc")
if [ $isInFile -eq 0 ];
then
  echo 'if [ -f '${distdir}'/OpenFOAM-'${ofoam_ver}'/etc/bashrc ]; then' >> ~/.bashrc
  echo '  . '${distdir}'/OpenFOAM-'${ofoam_ver}'/etc/bashrc' >> ~/.bashrc
  echo 'fi' >> ~/.bashrc
  $echo_e "${GRE}bashrc updated.${NC}"
else
  echo 'you should check your .bashrc file'
fi
# Evil hack to force reading the .bashrc file, escaping the first lines that escape in noninteractive shell..
eval "$(cat ~/.bashrc | tail -n +10)"
#
$echo_e "${GRE}VARENVS loaded.${NC}"

# build and configure third parties
$echo_e "${BLU}Compiling software.${NC}"
cd ThirdParty-${ofoam_ver}
./Allwmake -j 4 -k > log.make 2>&1
./makeParaView > log.paraview.make 2>&1
wmRefresh
cd ..
$echo_e "${GRE}Third parties built.${NC}"

# build openFoam
cd OpenFOAM-${ofoam_ver}
./Allwmake -j 4 -k > log.make 2>&1
$echo_e "${GRE}OpenFOAM version version ${ofoam_ver} built.${NC}"

# configure user env
mkdir -p ${distdir}/cedric-${ofoam_ver}
cd ${distdir}/cedric-${ofoam_ver}
mkdir -p $FOAM_RUN
$echo_e "${GRE}User environment version ${ofoam_ver} created.${NC}"

# test install
cd $FOAM_RUN
cp -r $FOAM_TUTORIALS/incompressible/simpleFoam/pitzDaily .
cd pitzDaily
blockMesh
simpleFoam
paraFoam

# install helyx-os
cd ${distdir}
./HELYX-OS-${helyxos_ver}-linux-x86_64.bin
echo 'export PATH=Engys/HELYX-OS/v'${helyxos_ver}':${PATH}' >> ~/.bashrc
# Evil hack to force reading the .bashrc file, escaping the first lines that escape in noninteractive shell..
eval "$(cat ~/.bashrc | tail -n +10)"